CC ?= gcc
LD ?= gcc

.PHONY: *

default: qc

qc: lint-config lint-container lint-twig dbschema phpcs phpstan psalm phpunit

phpunit:
	docker-compose exec app sh -c "XDEBUG_MODE=off vendor/bin/phpunit"

coverage:
	docker-compose exec app sh -c "XDEBUG_MODE=coverage vendor/bin/phpunit --coverage-html=var/coverage"

phpcs:
	docker-compose exec app sh -c "XDEBUG_MODE=off vendor/bin/phpcs -sp"

phpcbf:
	docker-compose exec app sh -c "XDEBUG_MODE=off vendor/bin/phpcbf -sp"

psalm:
	docker-compose exec app sh -c "XDEBUG_MODE=off vendor/bin/psalm --config=psalm.xml --show-info=true"

phpstan:
	docker-compose exec app sh -c "XDEBUG_MODE=off vendor/bin/phpstan analyse -c phpstan.neon"

deps:
	docker-compose exec app sh -c "XDEBUG_MODE=off composer outdated --direct"

lint-container:
	docker-compose exec app sh -c "XDEBUG_MODE=off bin/console lint:container"

lint-config:
	docker-compose exec app sh -c "XDEBUG_MODE=off bin/console lint:yaml config --parse-tags"

lint-twig:
	docker-compose exec app sh -c "XDEBUG_MODE=off bin/console lint:twig templates"

dbschema:
	docker-compose exec app sh -c "XDEBUG_MODE=off bin/console doctrine:schema:validate --skip-sync"

env.start:
	docker-compose up -d

env.stop:
	docker-compose down --remove-orphans

env.build:
	docker-compose down --remove-orphans
	docker-compose pull
	docker-compose build --no-cache --force-rm --pull

npm.install:
	docker-compose run node npm install

npm.audit:
	docker-compose run node npm audit

npm.audit.fix:
	docker-compose run node npm audit fix

encore.dev:
	docker-compose run node npm run dev

encore.build:
	docker-compose run node npm run build
